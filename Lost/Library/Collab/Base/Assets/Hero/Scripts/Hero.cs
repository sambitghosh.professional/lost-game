
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class Hero : MonoBehaviour
{
	public float m_MoveSpeed;
    public Animator animator;
    readonly string[] dialogs = {"1","2","3","4","5"};
    int next_dialog = 0;

    public int Health = 10;

    Rigidbody2D rb;

    public enum PlayerState { Alive, Dead }
    public PlayerState playerState = PlayerState.Alive;
    public Vector2 lookFacing;
	public Vector2 respawnPoint;
    AudioSource audioSource;
    float dashCooldown = 0f;
    public bool dead = false;
    public bool is_in_interactabe = false;
    public bool is_hit = false;
    public GameObject object_to_create;
    public GameObject current_interactable;
    public GameObject to_hit=null;
    public GameObject Textbox;

    void Start() {
        rb = GetComponent<Rigidbody2D>();
        animator.SetBool("alive", true);
        audioSource = GetComponent<AudioSource>();
    }
	void Update () 
	{
        if(playerState == PlayerState.Dead) {
            rb.velocity = Vector2.zero;
            return;
        }

		Vector3 tryMove = Vector3.zero;
        if (Input.GetKeyDown(KeyCode.E) && is_in_interactabe) checkbox();
        if (Input.GetKeyDown(KeyCode.F) && is_hit && to_hit!=null) attack();
        if (Input.GetKey(KeyCode.LeftArrow))
			tryMove += Vector3Int.left;
		if (Input.GetKey(KeyCode.RightArrow))
			tryMove += Vector3Int.right;
		if (Input.GetKey(KeyCode.UpArrow))
			tryMove += Vector3Int.up;
		if (Input.GetKey(KeyCode.DownArrow))
			tryMove += Vector3Int.down;

        rb.velocity = Vector3.ClampMagnitude(tryMove, 1f) * m_MoveSpeed;
        animator.SetBool("moving", tryMove.magnitude > 0);
        if (Mathf.Abs(tryMove.x) > 0) {
            animator.transform.localScale = tryMove.x < 0f ? new Vector3(-1f, 1f, 1f) : new Vector3(1f, 1f, 1f);
        }
        if(tryMove.magnitude > 0f) {
            lookFacing = tryMove;
        }

        if(Health <= 0)
        {
            dead = true;
        }
       

        dashCooldown = Mathf.MoveTowards(dashCooldown, 0f, Time.deltaTime);

        

        animator.SetBool("dash_ready", dashCooldown <= 0f);

	}

    //Will check for interactable items
    void OnTriggerEnter2D(Collider2D col)
    {
       
        if (col.tag == "Interactable")
        {
            is_in_interactabe = true;
            current_interactable = col.gameObject;
          
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
       
        if (col.tag == "Interactable")
        {
            is_in_interactabe = false;
            Debug.Log("EnteringExitcollider");
            Textbox.SetActive(false);
        }
    }

    //Will check for potential enemies in contact and allow engage
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag == "enemy")
        {
            to_hit = col.gameObject;
            is_hit = true;
            
        }
    }
    void OnCollisionExit2D(Collision2D col)
    {
        if (col.collider.tag == "Player1")
        {
            is_hit = false;
           
        }
    }

    public void LevelComplete() {
        Destroy(gameObject);
    }

    //Spawn gameObjects at positions of interactables.
    public void checkbox()
    {
        current_interactable.GetComponent<SpriteRenderer>().enabled = false;
        Textbox.SetActive(true);
        //Vector3 playerMove = GameObject.FindGameObjectWithTag("Interactable").transform.position;
        //Instantiate(object_to_create, playerMove, Quaternion.identity);
    }

    public void attack()
    {
        to_hit.GetComponent<npc>().health--;
        
    }
}
