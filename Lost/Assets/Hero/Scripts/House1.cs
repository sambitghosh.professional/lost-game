using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class House1 : MonoBehaviour
{
    public Text Text;

    public GameObject Textbox;

    public GameObject new_sprite;

    

    public string Monolog = "testing";

    public bool is_pole;

   

    public bool activate_dialog = false;

    public bool finish_dialog = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void activate_dialogs()
    {
        activate_dialog = true;
        finish_dialog = false;
       
        if (is_pole)
        {
            new_sprite.SetActive(true);
        }
        Text.text = Monolog;
    }

}
