using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class npc : MonoBehaviour
{
    public float m_MoveSpeed;
    Rigidbody2D rb;
    public int direction_counter;
    public int left = 1;
    public bool not_seen = true;
    public bool hit = false;
    public float attack_rate = 2.0f;
    public float next_attack_time = 0.0f;
    public bool hit_player = false;
    public int health = 10;
    bool dead = false;
    public bool isHurt = false;
    public GameObject player_ob;
    public bool is_boss;
    public string Monolog;
    public Text Text;

    public GameObject Textbox;

    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health < 0) { dead = true; }

        if (dead) Kill();


             Vector3 tryMove = Vector3.zero;
        if (not_seen == true)
        {

            if (left == 1 && direction_counter-- >= 0)
            {
                tryMove += Vector3Int.left;
                if (direction_counter == 0) { left = 0; direction_counter = 100; }
            }
            else if (left == 0 && direction_counter-- >= 0)
            {
                tryMove += Vector3Int.right;
                if (direction_counter == 0) { left = 1; direction_counter = 100; }
            }

            if (Mathf.Abs(tryMove.x) > 0)
            {
                animator.transform.localScale = tryMove.x < 0f ? new Vector3(1f, 1f, 1f) : new Vector3(-1f, 1f, 1f);
            }

            rb.velocity = Vector3.ClampMagnitude(tryMove, 1f) * m_MoveSpeed;
        }
        else   //Movement when player is seen
        {

            tryMove += Vector3Int.left;

            if (Mathf.Abs(tryMove.x) > 0)
            {
                animator.transform.localScale = tryMove.x < 0f ? new Vector3(1f, 1f, 1f) : new Vector3(-1f, 1f, 1f);
            }

            if (!hit)
                rb.velocity = Vector3.ClampMagnitude(tryMove, 1f) * m_MoveSpeed;
            else
            {
                rb.velocity = Vector3.ClampMagnitude(tryMove, 1f) * 0;

                if (hit_player && Time.time > next_attack_time)  //Delay enemey attacks by attack_rate time
                {
                    next_attack_time = Time.time + attack_rate;
                    attack_player();
                    isHurt = false;
                }
            }

        }



    }

    //Reduces player health on collision 
    public void attack_player()
    {

        player_ob.GetComponent<Hero>().Health = player_ob.GetComponent<Hero>().Health - 1;
        player_ob.GetComponent<Hero>().animator.SetTrigger("isHurt");
        animator.SetTrigger("isAttacking");

    }

    //Scans for player in vicinity to follow it
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "Player1")
        {

            not_seen = false;

        }
    }

    void OnTriggerExit2D(Collider2D col)
    {

    }

    //Triggers fight on collision with player and stops
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.tag == "Player1" || col.collider.tag == "enemy")
        {
            hit = true;
            if (col.collider.tag == "Player1")
            {
                hit_player = true;

            }

        }
    }

    //If player looses contact with enemy, enemy starts to move and stops attacking
    void OnCollisionExit2D(Collision2D col)
    {
        if (col.collider.tag == "Player1")
        {
            hit = false;
            hit_player = false;

        }
    }

    //Destroys the enemy on death
    public void Kill()
    {
        if (is_boss)
        {
            Text.text = Monolog;
            Textbox.SetActive(true);
        }
        if(dead)
        Destroy(gameObject);
        //dead = false;
    }

}
